//abhishek360
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = (sequelize, DataTypes) => {

  const User = sequelize.define('User',{
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastLogin: {
      type: DataTypes.DATE,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    verification: {
      type: DataTypes.ENUM('verified', 'pending'),
      defaultValue: 'pending',
    },
    status: {
      type: DataTypes.ENUM('active', 'inactive'),
      defaultValue: 'active',
    }
  });

  User.authenticate = async function( username, password ){
    const user = await User.findOne({ where: { username }});

    if(bcrypt.compareSync( password, user.password )) {
      user.update(
        {
          lastLogin: new Date()
        }
      );
      return user.authorize();
    }

    throw new Error('invalid password');
  }

  User.isTokenExpired = function(authToken) {
    const curr = new Date();
    const expiry = 30*60*1000;
    if(curr-authToken.createdAt>expiry){
      authToken.destroy();
      return true;
    }
    else return false;
  }

  User.getDetails = async function(userId) {
    const { UserDetails } = sequelize.models;
    const userDetails = await UserDetails.findOne({ where: { id: userId }});
    //console.log('userDetails-----------', userDetails);
    return {
      success: true,
      ...userDetails.dataValues,
    }
  }

  User.searchEmails = async function(email) {
    const { UserDetails } = sequelize.models;
    const userList = await UserDetails.findAll({
      where: {
        email: {
          [Op.like]: `%${email}%`
        }
      }
    });
    var list = [];
    userList.map((item)=>{
      const temp = {
        key: item.id,
        name: item.firstName,
        value: item.email,
      }
      list.push(temp);
    });
    console.log('user list-----------', list);
    return {
      success: true,
      list: list,
    }
  }

  User.getFriends = async function(userId) {
    const { UserDetails } = sequelize.models;
    const userDetails = await UserDetails.findOne({
      where: { id: userId },
    });
    const friendsSent = await userDetails.getFriendsSent();
    const friendsCame = await userDetails.getFriendsCame();
    var friendsAll = [];

    friendsSent.map((item)=>{
      const temp = {
        id: item.id,
        status: item.Friends.status,
        friendName: item.dataValues.firstName+' '+item.dataValues.lastName,
      }

      if(item.Friends.status==='confirmed')
        friendsAll.push(temp);
      //console.log('friends sent map', item.Friends);
    })

    friendsCame.map((item)=>{
      const temp = {
        id: item.id,
        status: item.Friends.status,
        friendName: item.dataValues.firstName+' '+item.dataValues.lastName,
      }

      if(item.Friends.status==='confirmed')
        friendsAll.push(temp);
      //console.log('friends came map', friendsCame);
    })
    console.log('friends all map', friendsAll);
    //console.log('friendssssssss senttttttttttttttt-----------', friendsSent, '\nfriendssssssss cameeeeeeeee------------------', friendsCame);
    return {
      success: true,
      friends: friendsAll,
    }
  }

  User.checkFriendship = async function(userId, friendId){
    const { Friends } = sequelize.models;
    const friendSent = await Friends.findOne({ where: {UserId: userId, friendId: friendId}});
    if(friendSent){
      if(friendSent.status==='pending'){
        return 0;
      }
      if(friendSent.status==='confirmed'){
        return 2;
      }
    }

    const friendRcvd = await Friends.findOne({ where: {UserId: friendId, friendId: userId}});
    if(friendRcvd){
      if(friendRcvd.status==='pending'){
        return 1;
      }
      if(friendRcvd.status==='confirmed'){
        return 2;
      }
    }

    return 3;
  }

  User.removeFriend = async function(userId, friendId){
    const { Friends } = sequelize.models;
    const friendSent = await Friends.destroy({ where: {UserId: userId, friendId: friendId}});
    if(friendSent){
      console.log('remove friend', frienndSent);
      return {
        success: true,
        message: 'Removed friend'
      }
    }

    const friendRcvd = await Friends.destroy({ where: {UserId: friendId, friendId: userId}});
    if(friendRcvd){
      console.log('remove friend', friendRcvd);
      return {
        success: true,
        message: 'Removed friend'
      }
    }

    return {
      success: false,
      message: 'friend does not exists',
    }
  }

  User.addFriendRequest = async function(UserId, friendId) {
    const { Friends } = sequelize.models;
    const createdAt = new Date();
    const friend = await Friends.findOne({ where: {UserId: friendId, friendId: UserId}});

    if(friend!==''){
      const friends = await Friends.create({
          friendId,
          createdAt,
          UserId,
        });
      return {
        success: true,
        ...friends.dataValues,
      }
    }
    else {
      return {
        success: false,
        ...friend.dataValues,
      }
    }
  }

  User.getFriendRequests = async function(userId) {
    const { UserDetails } = sequelize.models;
    const userDetails = await UserDetails.findOne({
      where: { id: userId },
    });
    const friendsCame = await userDetails.getFriendsCame();
    var friendsAll = [];
    friendsCame.map((item)=>{
      const temp = {
        id: item.id,
        status: item.Friends.status,
        friendName: item.dataValues.firstName+' '+item.dataValues.lastName,
      }

      if(item.Friends.status==='pending')
        friendsAll.push(temp);
      //console.log('friends came map', temp);
    })
    //console.log('friends all map', friendsAll);
    return {
      success: true,
      friends: friendsAll,
    }
  }

  User.acceptFriendRequest = async function(UserId, friendId) {
    const { Friends } = sequelize.models;
    const updatedAt = new Date();
    var friend = await Friends.findOne({ where: {UserId: friendId, friendId: UserId}});

    friend = await friend.update({
      status: 'confirmed',
      updatedAt
    });

    return {
      success: true,
      ...friend.dataValues,
    }
  }

  User.updateDetails = async function(token, lastName, firstName) {
    const { AuthToken, UserDetails } = sequelize.models;
    const authToken = await AuthToken.findOne({ where: { token }});
    //console.log('auth token',authToken.UserId);

    var userDetails = await UserDetails.findOne(
        { where: { UserId: authToken.UserId }}
      );

    await userDetails.update(
      {
        lastName,
        firstName
      }
    );

    return {
      success: true,
      ...userDetails.dataValues,
    }
  }

  User.prototype.addDetails = async function( details, t ) {
    const { UserDetails } = sequelize.models;
    const updatedAt = new Date();
    const userId = this.id;


    const userDetails = await UserDetails.create(
      Object.assign(
        details,
        { updatedAt, id: userId }
      ), {transaction: t});

    //await user.setUserDetails(userDetails);

    return userDetails;
  }

  User.prototype.authorize = async function() {
    //const { AuthToken } = sequelize.models;
    const user = this;

    const authToken = jwt.sign(
      {
        userId: user.id,
        username: user.username
      },
      process.env.JWT_SECRET,
      {
        expiresIn: '24h' // expires in 24 hours
      }
    );

    //await user.addAuthToken(authToken);

    return {
      user,
      authToken
    };
  }

  User.logout = async function(token) {
    //sequelize.models.AuthToken.destroy({ where: { token }});
    return true;
  }

  return User;
};
