//abhishek360

module.exports = (sequelize, DataTypes) => {
  const UserDetails = sequelize.define('UserDetails',{
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
    },
    gender: {
      type: DataTypes.ENUM('m', 'f'),
      allowNull: false,
    },
    dob: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    bio: {
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    updatedAt: {
      type: DataTypes.DATE,
    },
  });

  const Friends = sequelize.define('Friends',{
    status: {
      type: DataTypes.ENUM('pending', 'confirmed'),
      defaultValue: 'pending',
    },
    updatedAt: {
      type: DataTypes.DATE,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  })

  Friends.associate = function(models) {
    Friends.belongsTo(models.Users, { as: 'friendsSent', onDelete: 'CASCADE'});
    Friends.belongsTo(models.Users, { as: 'friendsCame', onDelete: 'CASCADE' });
  }

  UserDetails.associate = function(models) {
    UserDetails.belongsTo(models.User, { foreignKey: 'id', targetKey: 'id' });
    UserDetails.belongsToMany(UserDetails, {as: 'friendsSent', through: Friends, foreignKey: 'UserId'});
    UserDetails.belongsToMany(UserDetails, { as: 'friendsCame', through: Friends, foreignKey: 'friendId' });
  }

  return UserDetails;
};
