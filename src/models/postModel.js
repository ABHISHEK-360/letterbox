const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Post = new Schema(
  {
    userId: { type: String, reuired: true },
    userName: { type: String, reuired: true },
    title: { type: String, required: true },
    content: { type: String },
    likes: { type: Number },
  },
  {timestamps: true}
)

module.exports = mongoose.model('Post', Post);
