//abhishek360

var middleware = require('../middleware');
var bcrypt = require('bcrypt');
var express = require('express');
var router = express.Router();
var db = require('../models');
var { User } = require('../models');


router.post('/signup', async(req, res) => {

  const hash = bcrypt.hashSync(req.body.password, 10);

  db.sequelize.transaction((t) => {
    return User.create(
      Object.assign(
        { username: req.body.email, createdAt: new Date() },
        { password: hash }
      ), {transaction: t}).then((user) => {
        return user.addDetails(req.body, t);
      })
    }).then((result) => {
      return res.json(
        {
          success: true,
          message: 'User registered successfully.'
        }
      )
    }).catch((err) => {
      console.log('signup error-------************',err);
      //console.log('signup err--------',err.name, err.errors[0]);
      return res.status(400).send(err.errors[0].message);
    });

  // try {
  //   let user = await User.create(
  //     Object.assign({ username: req.body.email, createdAt: new Date() }, { password: hash })
  //   );
  //   //console.log('body--------',req.body);
  //
  //   //let data = await user.authorize();
  //   let details = await user.addDetails(req.body);
  //
  //   return res.json(
  //     {
  //       success: true,
  //       message: 'User registered successfully.'
  //     }
  //   );
  // }
  // catch(err){
  //   console.log('signup err--------',err);
  //   return res.status(400).send(err);
  // }
});

router.post('/login', async (req, res) => {
  const { username, password } = req.body;

  if(!username||!password){
    return res.status(400).send(
      'missing login details params'
    );
  }

  try {
    let user = await User.authenticate(username, password);

    return res.json(
      {
        success: true,
        id: user.user.id,
        verification: user.user.verification,
        username: user.user.username,
        token: user.authToken
      }
    );

  }
  catch(err) {
    console.log('err in login', err);
    return res.status(400).send(
      'invalid username or password'
    );
  }
})

router.put('/logout', async(req, res) => {
  const authToken = req.header('xyz-access-token');

  if(authToken) {
    const result = await User.logout(authToken);
    return res.send(
      {
        success: result,
        message: 'user logged out successfully!'
      }
    );
  }

  return res.status(400).send(
    {
      success: false,
      message: 'not authenticated'
    }
  );
})

router.get('/details', middleware.checkToken, async ( req, res ) => {
  //console.log('req in get details', req.decoded);
  let userId = req.decoded.userId;
  try {
    const userDetails = await User.getDetails(userId);
    return res.send(userDetails);
  } catch (e) {
    console.log('error in fetch details------------', e);
    return res.status(401).send(
      {
        success: false,
        message: 'invalid auth token or session expired',
      }
    );
  }
})

router.post('/search_user', middleware.checkToken, async ( req, res ) => {
  //console.log('req in get details', req.decoded);
  let userId = req.decoded.userId;
  const userEmail = req.body.email
  try {
    const userList = await User.searchEmails(userEmail);
    return res.send(
      userList
    );
  } catch (e) {
    console.log('error in fetch details------------', e);
    return res.status(400).send(
      {
        success: false,
        message: 'invalid data',
      }
    );
  }
})

router.post('/friend', middleware.checkToken, async (req, res) => {
  let userId = req.decoded.userId;
  const { friendId } = req.body;
  console.log('req in add_friend', req.body);
  try {
    const friend = await User.addFriendRequest(userId, friendId);
    return res.send(friend);
  } catch (e) {
    console.log('err in add_friend', e);
    return res.status(400).send(
      {
        success: false,
        message: 'invalid friend id',
      }
    );
  }
})

router.post('/accept_friend', middleware.checkToken, async (req, res) => {
  let userId = req.decoded.userId;
  const { friendId } = req.body;
  console.log('req in add_friend', req.body);
  try {
    const friend = await User.acceptFriendRequest(userId, friendId);
    if(!friend.success){
      return res.status(400).send(
        {
          success: false,
          message: 'already friends',
        }
      );
    }
    return res.send(friend);
  } catch (e) {
    console.log('err in add_friend', e);
    return res.status(400).send(
      {
        success: false,
        message: 'invalid friend id/already friends',
      }
    );
  }
})

router.post('/remove_friend', middleware.checkToken, async (req, res) => {
  let userId = req.decoded.userId;
  const { friendId } = req.body;
  console.log('req in remove friend', req.body);
  try {
    const friend = await User.removeFriend(userId, friendId);
    if(!friend.success){
      return res.status(400).send(
        {
          success: false,
          message: 'already friends',
        }
      );
    }
    return res.send(friend);
  } catch (e) {
    console.log('err in add_friend', e);
    return res.status(400).send(
      {
        success: false,
        message: 'invalid friend id/already friends',
      }
    );
  }
})

router.get('/friend', middleware.checkToken, async (req, res) => {
  let userId = req.decoded.userId;
  //console.log('req in get_friend', req.body);
  try {
    const friends = await User.getFriends(userId);
    return res.send(friends);
  } catch (e) {
    console.log('err in get_friend', e);
    return res.status(401).send(
      {
        success: false,
        message: 'invalid auth token or session expired',
      }
    );
  }
})

router.get('/friend_requests', middleware.checkToken, async (req, res) => {
  let userId = req.decoded.userId;
  //console.log('req in get_friend', req.body);
  try {
    const friends = await User.getFriendRequests(userId);
    return res.send(friends);
  } catch (e) {
    console.log('err in get_friend', e);
    return res.status(401).send(
      {
        success: false,
        message: 'invalid auth token or session expired',
      }
    );
  }
})

router.post('/update', async ( req, res ) => {
  const authToken = req.header('xyz-access-token');
  const { lastName, firstName } = req.body;

  if(authToken){
    try {
      const userDetails = await User.updateDetails(authToken, lastName, firstName);
      return res.send(userDetails);
    } catch (e) {
      return res.status(401).send(
        {
          success: false,
          message: 'invalid auth token'
        }
      );
    }
    //return res.send(userDetails);
  }
  return res.status(401).send(
    {
      success: false,
      message: 'missing auth token'
    }
  );
})

module.exports = router;
