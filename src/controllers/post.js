//abhishek360

var middleware = require('../middleware');
const path = require('path');
var bcrypt = require('bcrypt');
var express = require('express');
var router = express.Router();
const Post = require('../models/postModel');
var { User, Friends } = require('../models');

router.post('/timeline', middleware.checkToken, async (req, res) => {
  const userId = req.decoded.userId;
  const timelineId = req.body.timelineId;
  var timeline = {};
  try {
    const userDetails = await User.getDetails(timelineId);
    const friends = await User.getFriends(timelineId);
    var friendship = await User.checkFriendship(userId, timelineId);

    timeline = {
      ...userDetails,
      friends: friends.friends,
    }

    if(friendship!==''){
      timeline = {
        ...timeline,
        friendship
      }
    }
  } catch (e) {
    console.log('error in fetch timeline------------', e);
    return res.status(401).send(
      {
        success: false,
        message: 'invalid auth token or session expired',
      }
    );
  }

  await Post.find({ userId: timelineId}).exec(function (err, posts){
    if(err){
      console.log('get user posts', err);
      return res.status(500).send({
          success: false,
      });
    }

    if(posts){
      timeline = {
        ...timeline,
        posts
      }
    }
    else {
      timeline = {
        ...timeline,
        posts: [],
      }
    }
    console.log('timeline sent', timeline);
    return res.json({
      success: true,
      ...timeline
    });
  })
});

router.post('/post', middleware.checkToken, async (req, res) => {
  const userId = req.decoded.userId;
  var newPost = new Post(req.body);
  console.log(req.body);
  return await newPost.save(function (error) {
    if(error){
      console.log(error);
      return res.json(
        {
          success: false,
          message: 'Unable to save new post!'
        }
      );
    }
    else {
      return res.json(
        {
          success: true,
          message: 'New post added succesfully',
        }
      );
    }
  });
});

router.post('/remove_post', middleware.checkToken, async (req, res) => {
  const userId = req.decoded.userId;
  const postId = req.body.postId;
  const post = await Post.findOneAndDelete(
    { _id: postId, userId: userId }
  ).exec(function (err, post){
    if(err){
      console.log('remove post error', err);
      return res.res.status(400).json(
        {
          success: false,
          message: 'Unable to delete post!'
        }
      );
    }
    else{
      if(post){
        console.log('post deleted', post);
        return res.json(
          {
            success: true,
            message: 'Post deleted succesfully',
          }
        );
      }
      else{
        return res.status(400).json(
          {
            success: false,
            message: 'invalid details'
          }
        );
      }
    }
  })
});

router.get('/post', middleware.checkToken, async(req, res) => {
  let userId = req.decoded.userId;
  return await Post.find({}).exec(function (err, posts){
    if(err){
      return res.status(500).send(err);
    }

    return res.json({
      success: true,
      posts
    });
  })
});

router.get('/my_post', middleware.checkToken, async(req, res) => {
  let userId = req.decoded.userId;
  return await Post.find({ userId: userId}).exec(function (err, posts){
    if(err){
      return res.status(500).send(err);
    }

    return res.json({
      success: true,
      posts
    });
  })
});

module.exports = router;
