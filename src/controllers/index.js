//abhishek360

var express = require('express');
var router = express.Router();
var postController = require('./post');
var userController = require('./UserController');


router.use('/social', postController);
router.use('/users', userController);

// router.use('/api/users', (req,res) => {
//   console.log('request at /api/users on', new Date());
//   return new userController(req, res);
// });

module.exports = router;
