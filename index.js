//abhishek360
var jwt = require('jsonwebtoken');
var express = require('express');
var bodyParser = require('body-parser');
var env = require('dotenv').config();
var db = require('./db');
var models = require("./src/models");
var routes = require("./src/controllers");
var path = require('path');
var cors = require('cors');

var app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use('/api', routes);
app.use(express.static(path.join(__dirname, 'client/build')));

models.sequelize.sync().then(function(){
  console.log('Nice! Postgress Databse connected.')
}).catch(function(err) {
  console.log(err);
})

app.get('/*',function(req,res){
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

app.listen(process.env.PORT || 4000, function(err){
  if(!err){
    console.log('A360 listening on port 4000.');
  }
  else{
    console.log(err);
  }
})
