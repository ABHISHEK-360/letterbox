//abhishek360

import {
  GET_FRIENDS,
  GET_FRIENDS_SUCCESS,
  GET_FRIENDS_FAILURE,
  GET_FRIENDS_ERROR,
} from '../constants/action-constants';

const STATE = {
  friends:[
    {
      id: 0,
      status: '',
      friendName: '',
    },
  ],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case GET_FRIENDS:
      console.log('fetch friend list dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case GET_FRIENDS_SUCCESS:
      return {
        ...state,
        friends: action.payload.friends,
        isLoading: action.isLoading,
      };

    case GET_FRIENDS_FAILURE:
      console.log('fetch friend list failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case GET_FRIENDS_ERROR:
      console.log('fetch friend list error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
