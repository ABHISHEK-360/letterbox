//abhishek360

const STATE = {
  id: '',
  firstName: '',
  lastName: '',
  email:'',
  verification: '',
  phoneNumber:'',
  friendship: '',
  friends: [],
  posts: [],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case 'POST_TIMELINE':
      console.log('post timeline dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case 'POST_TIMELINE_SUCCESS':
      console.log('timeline', action.payload);
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case 'POST_TIMELINE_FAILURE':
      console.log('post timeline failure', action);
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case 'POST_TIMELINE_ERROR':
      console.log('post timeline error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
