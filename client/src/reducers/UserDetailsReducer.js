
const STATE = {
  user: {
    id: '',
    firstName: '',
    lastName: '',
    email:'',
    verification: '',
    phoneNumber:'',
  },
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case 'FETCH_USER_DETAILS':
      console.log('fetch user details dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case 'FETCH_USER_DETAILS_SUCCESS':
      console.log('user details', action.payload);
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case 'FETCH_USER_DETAILS_FAILURE':
      console.log('fetch user details failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case 'FETCH_USER_DETAILS_ERROR':
      console.log('fetch user details error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
