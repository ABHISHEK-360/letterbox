//abhishek360

import {
  GET_FRIEND_REQUESTS,
  GET_FRIEND_REQUESTS_SUCCESS,
  GET_FRIEND_REQUESTS_FAILURE,
  GET_FRIEND_REQUESTS_ERROR,
} from '../constants/action-constants';

const STATE = {
  requests:[
    {
      id: 0,
      status: '',
      friendName: '',
    },
  ],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case GET_FRIEND_REQUESTS:
      console.log('fetch friend request dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case GET_FRIEND_REQUESTS_SUCCESS:
      return {
        ...state,
        requests: action.payload.friends,
        isLoading: action.isLoading,
      };

    case GET_FRIEND_REQUESTS_FAILURE:
      console.log('fetch friend request failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case GET_FRIEND_REQUESTS_ERROR:
      console.log('fetch friend request error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
