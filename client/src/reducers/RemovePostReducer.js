//abhishek360

import {
  POST_REMOVE_POST,
  POST_REMOVE_POST_SUCCESS,
  POST_REMOVE_POST_FAILURE,
  POST_REMOVE_POST_ERROR,
} from '../constants/action-constants';

const STATE = {
  requests:[
    {
      id: 0,
      status: '',
      friendName: '',
    },
  ],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case POST_REMOVE_POST:
      console.log('remove post dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case POST_REMOVE_POST_SUCCESS:
      return {
        ...state,
        requests: action.payload.friends,
        isLoading: action.isLoading,
      };

    case POST_REMOVE_POST_FAILURE:
      console.log('remove post failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case POST_REMOVE_POST_ERROR:
      console.log('remove post error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
