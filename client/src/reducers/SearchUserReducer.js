//abhishek360

import {
  POST_SEARCH_USER,
  POST_SEARCH_USER_SUCCESS,
  POST_SEARCH_USER_FAILURE,
  POST_SEARCH_USER_ERROR,
} from '../constants/action-constants';

const STATE = {
  list:[
    {
      id: 0,
      status: '',
      friendName: '',
    },
  ],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case POST_SEARCH_USER:
      console.log('fetch user list dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case POST_SEARCH_USER_SUCCESS:
      return {
        ...state,
        list: action.payload.list,
        isLoading: action.isLoading,
      };

    case POST_SEARCH_USER_FAILURE:
      console.log('fetch user list failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case POST_SEARCH_USER_ERROR:
      console.log('fetch user list error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
