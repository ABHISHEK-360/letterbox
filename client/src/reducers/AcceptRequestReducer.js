//abhishek360

import {
  POST_ACCEPT_REQUEST,
  POST_ACCEPT_REQUEST_SUCCESS,
  POST_ACCEPT_REQUEST_FAILURE,
  POST_ACCEPT_REQUEST_ERROR,
} from '../constants/action-constants';

const STATE = {
  status: '',
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case POST_ACCEPT_REQUEST:
      console.log('post accept request dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case POST_ACCEPT_REQUEST_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case POST_ACCEPT_REQUEST_FAILURE:
      console.log('post accept request failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case POST_ACCEPT_REQUEST_ERROR:
      console.log('post accept request error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
