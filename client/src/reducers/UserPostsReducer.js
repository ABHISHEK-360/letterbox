//abhishek360

const STATE = {
  posts:[
    {
      _id: 0,
      userId: '',
      userName: '',
      title: '',
      content: '',
      likes:'',
    },
  ],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case 'FETCH_USER_POSTS':
      console.log('fetch user posts dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case 'FETCH_USER_POSTS_SUCCESS':
      return {
        ...state,
        posts: action.payload.posts,
        isLoading: action.isLoading,
      };

    case 'FETCH_USER_POSTS_FAILURE':
      console.log('fetch user posts failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case 'FETCH_USER_POSTS_ERROR':
      console.log('fetch user posts error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
