export const PRIMARY = '#053C5E' //DARK IMPERIAL BLUE
export const PRIMARY_SPECIAL = '#5941A9' //PLUMP PURPLE
export const SECONDARY = '#CE2D4F'; //DINGY_DUNGEON
export const BACKGROUND = '#BFDBF7' //PALE AQUA
export const FOREGROUND = '#A31621' //RUBY RED
export const FOREGROUND_2 = '#5DFDCB' //AQUAMARINE
export const DARK = '#1D1E18' //EERIE BLACK
export const LIGHT = '#E3C0D3' //THISTLE
export const LIGHT_SEC = '#DEC5E3' //LANGUID LAVENDER
export const SPECIAL_FONT = '#F15025' //FLAME
export const WHITE = '#ffffff'

export const QUICK_SILVER = '#A4A7B2';
export const OUTER_SPACE = '#424B54';
export const MAASTRICHT_BLUE = '#011936';
export const LAPIS_LAZULI = '#2274A5';
export const IMPERIAL = '#643173';
export const LIGHT_CRIMSON = '#EA638C';
