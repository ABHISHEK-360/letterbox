export const category = [
  {
    key: 1,
    value: 'photography',
    title: 'Photography',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/card_photo.jpg',
    desc: 'World of Photography!'
  },
  {
    key: 2,
    value: 'architecture',
    title: 'Architecture',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/card_arch.jpg',
    desc: 'World of Architecture!'
  },
  {
    key: 3,
    value: 'art',
    title: 'Art',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/card_art.jpg',
    desc: 'World of Art!'
  },
]

export const socialLinks = {
  fb: 'https://www.facebook.com/ourforum123/',
  insta: 'https://www.instagram.com/our.forum/',
}

export const homeSlide = [
  {
    key: 1,
    value: 'img1',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/slide1.jpg',
  },
  {
    key: 2,
    value: 'img2',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/slide2.jpg',
  },
  {
    key: 3,
    value: 'img3',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/slide3.jpg',
  },
]

export const photoEvents = [
  {
    title: 'photo event 1',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' photo event 1 desc'
  },
  {
    title: 'photo event 2',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'photo event 2 desc'
  },
  {
    title: 'photo event 3',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'photo event 3 desc'
  },
  {
    title: 'photo event 4',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' photo event 1 desc'
  },
]

export const photoSlides = [
  {
    key: 1,
    value: 'img1',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/photography_assets/photo_slide_1.jpg',
  },
  {
    key: 2,
    value: 'img2',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/photography_assets/photo_slide_2.jpg',
  },
  {
    key: 3,
    value: 'img3',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/photography_assets/photo_slide_3.jpg',
  },
]

export const archiSlides = [
  {
    key: 1,
    value: 'img1',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/archi_assets/archi_slide_1.jpg',
  },
  {
    key: 2,
    value: 'img2',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/archi_assets/archi_slide_2.jpg',
  },
  {
    key: 3,
    value: 'img3',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/archi_assets/archi_slide_3.jpg',
  },
]

export const artSlides = [
  {
    key: 1,
    value: 'img1',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/art_assets/art_slide_1.jpg',
  },
  {
    key: 2,
    value: 'img2',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/art_assets/art_slide_2.jpg',
  },
  {
    key: 3,
    value: 'img3',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/art_assets/art_slide_3.jpg',
  },
]

export const archiEvents = [
  {
    title: 'archi event 1',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' archi event 1 desc'
  },
  {
    title: 'archi event 2',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'archi event 2 desc'
  },
  {
    title: 'archi event 3',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'archi event 3 desc'
  },
  {
    title: 'archi event 4',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' archi event 1 desc'
  },
]

export const artsEvents = [
  {
    title: 'arts event 1',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' arts event 1 desc'
  },
  {
    title: 'arts event 2',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'arts event 2 desc'
  },
  {
    title: 'arts event 3',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'arts event 3 desc'
  },
  {
    title: 'arts event 4',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' arts event 1 desc'
  },
]
