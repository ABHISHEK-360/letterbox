//abhishek360

import React from 'react';
import {
  Typography,
  Card,
  Grid,
  IconButton,
  CardActionArea,
} from '@material-ui/core'
import * as Colors from '../configs/colors';
import {
  Delete,
  Edit,
  Share,
  ThumbUp,
} from '@material-ui/icons';

//["h1","h2","h3","h4","h5","h6","subtitle1","subtitle2",
//"body1","body2","caption","button","overline","srOnly","inherit"]
class PostCard extends React.Component {
  render() {
    const item = this.props.item;
    return (
      <div align = 'left' style = { styles.container }>
        <Card style = {this.props.cardStyle}
          onClick = {this.props.onClick}
        >
            <div style = {styles.topContainer}>
              <Typography
                variant = 'body1'
                style = {{color: Colors.PRIMARY}}
              >
                Posted by { item.userName } at { item.createdAt } { /*item.createdAt.split('T')[1].split('/.')[0]*/ }
              </Typography>
              <Typography
                variant = 'body2'
              >
                { item.title }
              </Typography>
              <Typography
                variant = 'caption'
              >
                { item.content }
              </Typography>
            </div>
              <div style = {styles.bottomContainer}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={9}>
                    <Typography
                      variant = 'caption'
                      style = {styles.bottomText}
                    >
                      Likes: { item.likes }
                    </Typography>
                    <Typography
                      gutterBottom
                      variant = 'caption'
                      style = {styles.bottomText}
                    >
                      Comments: 10
                    </Typography>
                    <Typography
                      gutterBottom
                      variant = 'caption'
                      style = {styles.bottomText}
                    >
                      Share: 15
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={3}>
                  { this.props.flag === 'profile' &&
                      <div>
                        <IconButton
                          size = 'small'
                          style = {styles.iconButton}
                          edge="end"
                          aria-label="delete"
                        >
                          <Edit/>
                        </IconButton>
                        <IconButton
                          size = 'small'
                          style = {styles.iconButton}
                          edge="end"
                          aria-label="delete"
                          onClick = {(event) => this.props.removePost(item._id)}
                        >
                          <Delete/>
                        </IconButton>
                      </div>
                  }
                  { this.props.flag === 'home' &&
                    <div>
                      <IconButton
                        size = 'small'
                        style = {styles.iconButton}
                        edge="end"
                        aria-label="delete"
                      >
                        <ThumbUp/>
                      </IconButton>
                      <IconButton
                        size = 'small'
                        style = {styles.iconButton}
                        edge="end"
                        aria-label="delete"
                      >
                        <Share/>
                      </IconButton>
                    </div>
                  }
                  </Grid>
                </Grid>
              </div>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {
    marginTop: 10,
    marginRight: '25%',
    width: '75%'
  },
  topContainer: {
    paddingLeft: 10,
    paddingTop: 5,
  },
  bottomText: {
    fontSize: 14,
    marginLeft: 20,
  },
  iconButton: {
    padding: 2,
    marginRight: 10,
    backgroundColor: Colors.FOREGROUND
  },
  bottomContainer: {
    paddingTop: 5,
    paddingLeft: 5,
    paddingBottom: 5,
    color: Colors.WHITE,
    backgroundColor: Colors.PRIMARY_SPECIAL,
  },
  actionAreaCard: {
  }
};


export default PostCard;
