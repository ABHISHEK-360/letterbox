//abhishek360

import React, { Component } from 'react';
import {  Link } from "@reach/router";
import{
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core';
import * as Colors from '../configs/colors'


class NavBar extends Component {
  render(){
    return (
      <List component = 'nav'>
        <ListItem component = 'div'>
          <ListItemText inset>
            <Link getProps = {({isCurrent} ) =>{
              return {
                style:{
                  ...styles.linkText,
                  color: isCurrent ? Colors.SPECIAL_FONT : Colors.WHITE,
                }
              }
            }}

            to="/"
            >
              Home
            </Link>
          </ListItemText>
          <ListItemText inset>
            <Link to="aboutus" getProps = {({isCurrent} ) =>{
              return {
                style:{
                  ...styles.linkText,
                  color: isCurrent ? Colors.SPECIAL_FONT : Colors.WHITE,
                }
              }
            }}
            >
              About Us
            </Link>
          </ListItemText>
        </ListItem>
      </List>
    )
  }
}

const styles = {
  linkText: {
    fontSize: 18,
    textDecoration: 'none',
  },
}

export default NavBar;
