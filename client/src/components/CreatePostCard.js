//abhishek360

import React from 'react';
import {
  Typography,
  Card,
  Button,
  TextField,
  CardActionArea,
} from '@material-ui/core'
import * as Colors from '../configs/colors';

//["h1","h2","h3","h4","h5","h6","subtitle1","subtitle2",
//"body1","body2","caption","button","overline","srOnly","inherit"]
class CreatePostCard extends React.Component {
  state = {
      title: "",
      content: "",
  }

  verifyCreatePost = () => {
    const { title, content } = this.state;
    if(title===''){
      alert('Title is Required!');
      return;
    }
    else if(content===''){
      alert('Please provide some content!');
      return;
    }
    else{
      this.props.updatePostDetails(title, content);
    }

  }

  render() {
    return (
      <div align = 'left' style = { styles.container }>
        <Card style = {styles.cardStyle}>
            <div style = {styles.topContainer}>

              <Typography
                variant = 'caption'
                style = {{fontSize: 18}}
              >
                Title:
              </Typography>
              <TextField
                placeholder="Provide a title of the post."
                style = { styles.textField }
                onChange = {(event) => this.setState({ title: event.target.value})}
              />
              <br/>
              <Typography
                variant = 'caption'
                style = {{fontSize: 18}}
              >
                Content:
              </Typography>
              <TextField
                placeholder="Provide content of the post."
                style = { styles.textField }
                onChange = {(event) => this.setState({content: event.target.value})}
              />
            </div>
              <div align = 'right' style = {styles.bottomContainer}>
                <Button
                  variant = 'outlined'
                  size = 'small'
                  style = {styles.postButton}
                  onClick = {()=> this.verifyCreatePost()}
                >
                  Create Post
                </Button>
              </div>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {
    marginTop: 10,
    marginRight: '25%',
    width: '75%'
  },
  cardStyle: {
    borderRadius: 0,
  },
  topContainer: {
    padding: 5
  },
  textField:{
    marginLeft: 10,
  },
  bottomText: {
    marginLeft: 20,
  },
  bottomContainer: {
    padding: 5,
    color: 'white',
    backgroundColor: Colors.FOREGROUND_2,
  },
  postButton:{
    marginLeft: 10,
    paddingLeft: 30,
    paddingRight: 30,
    marginRight: 10,
    width: '20',
    color: Colors.PRIMARY_SPECIAL,
  },
  actionAreaCard: {
  }
};


export default CreatePostCard;
