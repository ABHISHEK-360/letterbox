//abhishek360
import React, { Component } from 'react';
import {
  Card,
  Typography,
  Grid,
  Button,
  CardActionArea,
  CardMedia,
  CardContent
} from '@material-ui/core'
import * as Colors from '../configs/colors';
import { connect } from 'react-redux';

import FriendsPanel from './FriendsPanel';
import PhotosPanel from './PhotosPanel';
import PostCard from './PostCard';
import {
  getFriends,
} from '../actions/FriendStateActions';
import {
  fetchUserPosts,
} from '../actions/UserStateActions';
import {
  postRemovePost,
} from '../actions/PostStateActions';

class UserProfile extends Component{
  componentDidMount() {
    this.props.getFriends();
    this.props.fetchUserPosts();
  }

  render(){
    const user = this.props.userDetails;
    const posts = this.props.userPosts.posts;
    return (
      <div
        style = {styles.container}
      >
        <Card style = {{ backgroundImage: "linear-gradient(to top, #020024, #c90d3d, #020024 )", marginLeft: '20.5%', width: '59%'}}>
            <CardContent align = 'center' style = {styles.cardContentTop}>
              <Typography
                gutterBottom
                style = {{fontSize: 35, color: Colors.WHITE}}
                variant = 'caption'
              >
                {user.firstName+" "+user.lastName}
              </Typography>
              <CardMedia
                component = "img"
                alt = "asdfghj"
                style = {{margin: 5, backgroundColor: Colors.WHITE, border: '4px solid #020024' , borderRadius: 100, height: 200, width: 200 }}
                image = "https://img.icons8.com/wired/128/000000/circled-user.png"
                tittle = 'Photography'
              />
              <br/>
              <Typography
                gutterBottom
                style = {{fontSize: 20, color: Colors.WHITE}}
                variant = 'caption'
              >
                User Short Bio
              </Typography>
            </CardContent>
            <CardContent style = {styles.cardContent}>
              <Typography
                gutterBottom
                variant = 'caption'
                style = {{fontSize: 16}}
              >
                Email: {user.email}
              </Typography>
              <br/>
              <Typography
                gutterBottom
                variant = 'caption'
                style = {{fontSize: 16,}}
              >
                Phone: {user.phone}
              </Typography>
              <br/>
              <Typography
                gutterBottom
                variant = 'caption'
                style = {{fontSize: 16,}}
              >
                Gender: {user.gender==='m'? 'Male' : 'Female'}
              </Typography>
            </CardContent>
        </Card>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={5}>
            <PhotosPanel user = {this.props.userDetails}/>
            <br/>
            <FriendsPanel friends = {this.props.friendList.friends}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            {
              posts.map(item => {
                return (
                  <PostCard
                    key = {item._id}
                    flag = 'profile'
                    removePost = {(postId) => this.props.postRemovePost(postId)}
                    cardStyle = {styles.mobileCard}
                    item = {item}
                  />
                )
              })
            }
          </Grid>
        </Grid>
      </div>
    );
  }
}

const styles = {
  container: {
    paddingTop: 40,
    width: '100vw'
  },
  cardContent: {
    backgroundColor: Colors.FOREGROUND_2,
  },
  cardContentTop: {
    alignItems: 'center',
  }
};

const mapStateToProps = ({ userDetails, userPosts, friendList }) => ({ userDetails, userPosts, friendList });

export default connect(mapStateToProps, {
  postRemovePost,
  fetchUserPosts,
  getFriends,
})(UserProfile);
