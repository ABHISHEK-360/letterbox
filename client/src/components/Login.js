//abhishek360

import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { navigate } from '@reach/router';
import * as Colors from '../configs/colors';

class Login extends Component{
  state = {
    username: '',
    password: '',
  }

  handleClick = (event) => {
    alert('button clicked');
  }

  handleTextChange = (event) => {
    const newValue = event.target.value;
    const id = event.target.id;
    // console.log('handleTextChange', id, newValue );
    // this.setState({
    //   username: newValue,
    // })

    switch (id) {
      case 'usernameTextFieldLogin':
        this.setState({
          username: newValue,
        })
        break;
      case 'passwordTextFieldLogin':
        this.setState({
          password: newValue,
        })
        break;
      default:

    }
  }

  render(){
    const { username, password } = this.state;
    return (
      <div
      >
        <div
          style = { styles.head }
        >
          <h1 align = "center"> Log In </h1>
          <h4 align = "center">Missing your Social Life? Let's connect.</h4>
        </div>
        <div
          align = 'center'
          style={styles.container}
        >
          <TextField
            id="usernameTextFieldLogin"
            fullWidth
            style = { styles.textField }
            label="Username"
            onChange = {(event) => this.handleTextChange(event)}
          />
          <TextField
            type="password"
            style = { styles.textField }
            id="passwordTextFieldLogin"
            label="Password"
            onChange = {(event) => this.handleTextChange(event)}
          />
        <div
          align = 'center'
        >
        <br/>
        <br/>
        <Button
          style={styles.button}
          onClick={(event) => this.props.handleLogin(username, password)}
        >
          Login
        </Button>
        </div>
        </div>
        <div
          style = {styles.footer}
        >
          <div
            align = 'center'
          >
          Not yet Registered?
          <Button
            style={styles.footerButton}
            onClick={(event) => {
                this.props.closePopup();
                navigate('register')
              }
            }
          >
            Sign Up
          </Button>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  container: {
    position: 'absolute',
    width: '100%',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)'
  },
  head: {
    top: '10%',

  },
  textField: {
    width: '75%',
    margin: 10,
  },
  button: {
    margin: 10,
    width: '75%',
    color: 'white',
    background: Colors.FOREGROUND,
  },
  footer: {
    position: 'absolute',
    bottom: '0%',
    width: '100%',
    borderRadius: 5,
    backgroundColor: Colors.LIGHT,
  },
  footerButton: {
    margin: 10,
    color: 'white',
    background: Colors.FOREGROUND,
  },
};



export default Login;
