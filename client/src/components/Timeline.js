//abhishek360
import React, { Component } from 'react';
import {
  Card,
  Typography,
  Grid,
  Button,
  CardActionArea,
  CardMedia,
  CardContent
} from '@material-ui/core'
import * as Colors from '../configs/colors';
import { connect } from 'react-redux';

import FriendsPanel from './FriendsPanel';
import PhotosPanel from './PhotosPanel';
import PostCard from './PostCard';
import {
  postTimeline,
} from '../actions/UserStateActions';
import {
  postAddFriend,
  postRemoveFriend,
  postAcceptRequest,
} from '../actions/FriendStateActions';

class Timeline extends Component{
  componentDidMount() {
    console.log('props timeline', this.props.timelineId);
    const timelineId = this.props.timelineId;
    this.props.postTimeline(timelineId);
  }

  renderFriendship = () => {
    const { friendship, id } = this.props.timeline;
    switch(friendship){
      case 0 :
        return (
          <div>
            <Button
              onClick={(event) => null}
              style = {styles.button}
            >
              Request Pending
            </Button>
            <Button
              onClick={(event) => this.props.postRemoveFriend(id)}
              style = {styles.button}
            >
              Cancel Request
            </Button>
          </div>
        );

      case 1:
        return (
          <div>
            <Button
              onClick={(event) => this.props.postAcceptRequest(id)}
              style = {styles.button}
            >
              Accept
            </Button>
            <Button
              onClick={(event) => this.props.postRemoveFriend(id)}
              style = {styles.button}
            >
              Decline
            </Button>
          </div>
        );

      case 2:
        return (
          <div>
            <Button
              onClick={(event) => null}
              style = {styles.button}
            >
              Friends
            </Button>
            <Button
              onClick={(event) => this.props.postRemoveFriend(id)}
              style = {styles.button}
            >
              Unfriend
            </Button>
          </div>
        );

      case 3:
        return (
          <div>
            <Button
              onClick={(event) => this.props.postAddFriend()}
              style = {styles.button}
            >
              Add as Friend
            </Button>
          </div>
        );

      default:
        return (
          <div></div>
        );
    }
  }

  render(){
    const user = this.props.timeline;
    const { posts, friends } = this.props.timeline;
    return (
      <div
        style = {styles.container}
      >
        <Card style = {{ backgroundImage: "linear-gradient(to top, #020024, #c90d3d, #020024 )", marginLeft: '20.5%', width: '59%'}}>
          <CardContent align = 'center' style = {styles.cardContentTop}>
            <CardMedia
              component = "img"
              alt = "asdfghj"
              style = {{margin: 5, backgroundColor: Colors.WHITE, border: '4px solid #020024' , borderRadius: 100, height: 200, width: 200 }}
              image = "https://img.icons8.com/wired/128/000000/circled-user.png"
              tittle = 'Photography'
            />
            <Typography
              gutterBottom
              style = {{fontSize: 30, color: Colors.WHITE}}
              variant = 'caption'
            >
              {user.firstName+" "+user.lastName}
            </Typography>
            <br/>
            <Typography
              gutterBottom
              style = {{fontSize: 20, color: Colors.WHITE}}
              variant = 'caption'
            >
              User Short Bio {user.bio}
            </Typography>
          </CardContent>
          <CardContent style = {styles.cardContent}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <Typography
                  gutterBottom
                  variant = 'caption'
                  style = {{fontSize: 16,}}
                >
                  Email: {user.email}
                </Typography>
                <br/>
                <Typography
                  variant = 'caption'
                  style = {{fontSize: 16,}}
                >
                  Gender: {user.gender==='m'? 'Male' : 'Female'}
                </Typography>
              </Grid>
              <Grid style = {{paddingLeft: '10%'}} item xs={12} sm={6}>
                <Grid container spacing={0}>
                  <Grid item xs={12} sm={2}>
                  </Grid>
                  <Grid item xs={12} sm={10}>
                    {this.renderFriendship()}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={5}>
            <PhotosPanel user = {user}/>
            <br/>
            <FriendsPanel friends = {friends}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            {
              posts.map(item => {
                return (
                  <PostCard
                    key = {item._id}
                    cardStyle = {styles.mobileCard}
                    item = {item}
                  />
                )
              })
            }
          </Grid>
        </Grid>
      </div>
    );
  }
}

const styles = {
  container: {
    paddingTop: 40,
    width: '100vw'
  },
  button: {
    marginLeft: 10,
    marginRight: 10,
    width: '20',
    color: 'white',
    background: Colors.FOREGROUND,
  },
  cardContent: {
    backgroundColor: Colors.FOREGROUND_2,
  },
  cardContentTop: {
    alignItems: 'center',
  }
};

const mapStateToProps = ({ timeline }) => ({ timeline });

export default connect(mapStateToProps, {
  postAddFriend,
  postRemoveFriend,
  postAcceptRequest,
  postTimeline,
})(Timeline);
