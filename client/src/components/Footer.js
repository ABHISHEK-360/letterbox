//abhishek360

import React from 'react';
import {
  Typography,
} from '@material-ui/core';
import * as Colors from '../configs/colors';

class Footer extends React.Component {
  render() {
    return (
      <div style = { styles.container }>
        <Typography
          style = {styles.textField}
          align = 'right'
        >
          Developed & Maintained By: Abhishek360
        </Typography>

      </div>
    );
  }
}

const styles = {
  container: {
    position: 'fixed',
    bottom: 0,
    width: '100%',
    paddingTop: 5,
    paddingLeft: 5,
    paddingBottom: 5,
    backgroundColor: Colors.PRIMARY,
  },
  textField: {
    fontSize: 16,
    marginRight: 10,
    color: Colors.SPECIAL_FONT,
  },
};

export default Footer;
