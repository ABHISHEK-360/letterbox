//abhishek360

import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { Close  } from '@material-ui/icons';
import Login from './Login';
import './style.css';

class Popup extends React.Component {
  render() {
    return (
      <div className = 'popup'>
        <div className = 'popup_inner'>
          <div className = 'popup_close'>
            <IconButton
              onClick={this.props.closePopup}
            >
              <Close/>
            </IconButton>
          </div>
          {
            (this.props.type === 'login') &&
              <Login
                closePopup = {this.props.closePopup}
                handleLogin = { this.props.handleLogin }
                changePopupType = { this.props.changePopupType }
              />
          }
        </div>
      </div>
    );
  }
}


export default Popup;
