//abhishek360

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PostCard from './PostCard';
import CreatePostCard from './CreatePostCard';
import RequestsPanel from './RequestsPanel';
import ProfilePanel from './ProfilePanel';
import {
  Grid,
} from '@material-ui/core'
import {
  fetchPosts,
  postCreatePost,
} from '../actions/PostStateActions';
import {
  postAcceptRequest,
  postRemoveFriend,
  getFriendRequests,
} from '../actions/FriendStateActions';
import {
  fetchUserDetails,
} from '../actions/UserStateActions';
import logo from '../assests/letterBox_logo.png';

class Home extends Component {
  componentDidMount() {
    this.props.fetchPosts();
    this.props.getFriendRequests();
  }

  updatePostDetails = (title, content) => {
    const user = this.props.userDetails;
    this.props.postCreatePost(user.id, user.firstName+" "+user.lastName, title, content, 10 )
  }

  render(){
    const posts = this.props.posts.posts;
    return (
      <div style = {styles.container}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={5}>
            <ProfilePanel user = {this.props.userDetails}/>
            <br/>
            <RequestsPanel
              requests = {this.props.friendReq.requests}
              acceptRequest = {this.props.postAcceptRequest}
              declineRequest = {this.props.postRemoveFriend}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <CreatePostCard updatePostDetails = {this.updatePostDetails} />
            {
              posts.map(item => {
                return (
                  <PostCard
                    key = {item._id}
                    flag = 'home'
                    cardStyle = {styles.mobileCard}
                    item = {item}
                  />
                )
              })
            }
          </Grid>
        </Grid>
      </div>
    );
  }
}

const styles = {
  container: {
    paddingTop: 30,
  },
  actionAreaCard: {
  },
  mobileCard: {
    borderRadius: 0,
  },
};

const mapStateToProps = ({ userDetails, posts, friendReq }) => ({ userDetails, posts, friendReq });

export default connect(mapStateToProps, {
    fetchPosts,
    postAcceptRequest,
    postRemoveFriend,
    getFriendRequests,
    postCreatePost,
    fetchUserDetails,
  })(Home);
