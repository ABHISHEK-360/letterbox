//abhishek360

import React from 'react';
import {
  Typography,
  Card,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
} from '@material-ui/core';
import { Link } from '@reach/router';
import {
  Face,
} from '@material-ui/icons';
import * as Colors from '../configs/colors';

//["h1","h2","h3","h4","h5","h6","subtitle1","subtitle2",
//"body1","body2","caption","button","overline","srOnly","inherit"]
class FriendsPanel extends React.Component {
  render() {
    console.log('friends in panel', this.props.friends);
    return (
      <div align = 'left' style = { styles.container }>
        <Card style = {styles.cardStyle}>
            <div style = {styles.topContainer}>
              <Typography
                variant = 'caption'
                style = {{ color: Colors.WHITE, fontSize: 18}}
              >
                Freinds
              </Typography>
            </div>
              <List style={{maxHeight: '100%', overflow: 'auto'}}>
                {this.props.friends.map(item => (
                  <Card key={item.id} style = {styles.itemCardStyle}>
                    <ListItem>
                      <ListItemAvatar>
                        <Avatar>
                          <Face />
                        </Avatar>
                      </ListItemAvatar>
                      <Link
                          style = {styles.linkText}
                          to= {'/timeline/'+item.id}
                        >
                          {item.friendName}
                      </Link>
                    </ListItem>
                  </Card>
                ))}
              </List>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {
    marginTop: 10,
    marginLeft: '50%',
    width: '50%'
  },
  cardStyle: {
    borderRadius: 2,
  },
  itemCardStyle: {
    margin: 5,
    borderRadius: 2,
    backgroundColor: Colors.FOREGROUND_2,
  },
  linkText: {
    fontSize: 15,
    textDecoration: 'underline',
    color: Colors.SPECIAL_FONT
  },
  topContainer: {
    backgroundColor: Colors.PRIMARY_SPECIAL,
    padding: 5
  },
  textField:{
    marginLeft: 10,
  },
  bottomText: {
    marginLeft: 20,
  },
  bottomContainer: {
    padding: 5,
    color: 'white',
    backgroundColor: Colors.LIGHT_SEC,
  },
  postButton:{
    marginLeft: 10,
    paddingLeft: 30,
    paddingRight: 30,
    marginRight: 10,
    width: '20',
    color: Colors.PRIMARY_SPECIAL,
  },
  actionAreaCard: {
  }
};

export default FriendsPanel;
