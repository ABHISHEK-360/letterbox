//abhishek360

import React, { Component } from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AuthHOC from '../HOC/AuthHOC';
import { connect } from 'react-redux';
import { navigate, Link } from '@reach/router';
import logo from '../assests/letterbox.png';
import NavBar from './NavBar.js'
import * as Colors from '../configs/colors'
import ReactSearchBox from 'react-search-box';
import {
  postSearchUsers,
} from '../actions/UserStateActions';

class Header extends Component{
  data = [
    {
      key: 'john',
      value: 'John Doe',
    },
    {
      key: 'jane',
      value: 'Jane Doe',
    },
    {
      key: 'mary',
      value: 'Mary Phillips',
    },
    {
      key: 'robert',
      value: 'Robert',
    },
    {
      key: 'karius',
      value: 'Karius',
    },
  ]

  render(){
    const { email, firstName } = this.props.userDetails;
    return(
      <div style = { styles.container } >
        <AppBar
          style = { styles.appBarContainer }
        >
          <Toolbar>
          <div align = 'center' style={styles.logoImg}>
            <img src = {logo} height= {30} width= {150} alt="letterBox"/>
          </div>
          <div>
            <ReactSearchBox
              placeholder="Type Emails..."
              data={this.props.searchList.list}
              onSelect = {(item) => navigate('/timeline/'+item.key)}
              onChange = {(value) => this.props.postSearchUsers(value)}
              callback={record => console.log(record)}
            />
          </div>
          <div style = {{flex: 5}}>
            <NavBar/>
          </div>
          <AuthHOC
            yes = {() =>
              <div align = 'center' style = {{ flex: 3 }}>
                {
                  email &&
                    <Typography
                      variant = 'caption'
                      style = {{fontSize: 16, color: Colors.WHITE}}
                    >
                      Hello, <Link
                                style = {styles.linkText}
                                to="userprofile"
                              >
                                { firstName.toUpperCase() }
                              </Link>
                    </Typography>
                }
                <Button
                  id = 'headerLogoutButton'
                  style={styles.button}
                  onClick={(event) => this.props.handleLogout()}
                >
                  Logout
                </Button>
              </div>

            }
            no = {() =>
              <div align = 'center' style = {{ flex: 3 }}>
                <Button
                  id = 'headerLoginButton'
                  style={styles.button}
                  onClick={(event) => this.props.togglePopup('login')}
                >
                  Login
                </Button>
                <Button
                  id = 'headerRegisterButton'
                  style={styles.button}
                  onClick={(event) => navigate('register')}
                >
                  SIGN UP
                </Button>
              </div>
            }
          />
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

const styles = {
  container: {
    position: 'fixed',
    top: 0,
    zIndex: 5,
    width: '100%',
  },
  linkText: {
    fontSize: 15,
    textDecoration: 'underline',
    color: Colors.SPECIAL_FONT
  },
  appBarContainer: {
    width: '100%',
    color: '#A4A7B2',
    display: 'flex',
    backgroundColor: Colors.PRIMARY,
  },
  logoImg: {
    flex: 4,
    borderRadius: 5,
  },
  textField: {
    width: '20',
    margin: 10,
  },
  button: {
    marginLeft: 10,
    marginRight: 10,
    width: '20',
    color: 'white',
    background: Colors.FOREGROUND,
  },
};

const mapStateToProps = ({ userDetails, searchList }) => ({ userDetails, searchList });

export default connect(mapStateToProps, {
  postSearchUsers,
  })(Header);
