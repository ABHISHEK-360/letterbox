//abhishek360

import React from 'react';
import {
  Typography,
  Card,
  CardContent,
  CardMedia,
  Button,
  TextField,
  CardActionArea,
} from '@material-ui/core'
import * as Colors from '../configs/colors';

//["h1","h2","h3","h4","h5","h6","subtitle1","subtitle2",
//"body1","body2","caption","button","overline","srOnly","inherit"]
class ProfilePanel extends React.Component {
  render() {
    const user = this.props.user;
    return (
      <div align = 'left' style = { styles.container }>
        <Card style = {{ backgroundImage: "linear-gradient(to top, #020024, #c90d3d, #020024 )"}}>
          <CardContent align = 'center' style = {styles.cardContentTop}>
            <CardMedia
              component = "img"
              alt = "asdfghj"
              style = {{margin: 5, backgroundColor: Colors.WHITE, border: '4px solid #020024' , borderRadius: 100, height: 200, width: 200 }}
              image = "https://img.icons8.com/wired/128/000000/circled-user.png"
              tittle = 'Photography'
            />
            <br/>
            <Typography
              gutterBottom
              style = {{fontSize: 35, color: Colors.WHITE}}
              variant = 'caption'
            >
              {user.firstName+" "+user.lastName}
            </Typography>
            <br/>
            <Typography
              gutterBottom
              variant = 'caption'
              style = {{fontSize: 16, color: 'white'}}
            >
              Email: {user.email}
            </Typography>
          </CardContent>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {
    marginTop: 10,
    marginLeft: '50%',
    width: '50%'
  },
  cardStyle: {
    borderRadius: 0,
  },
  topContainer: {
    padding: 5
  },
  textField:{
    marginLeft: 10,
  },
  bottomText: {
    marginLeft: 20,
  },
  bottomContainer: {
    padding: 5,
    color: 'white',
    backgroundColor: Colors.LIGHT_SEC,
  },
  postButton:{
    marginLeft: 10,
    paddingLeft: 30,
    paddingRight: 30,
    marginRight: 10,
    width: '20',
    color: Colors.PRIMARY_SPECIAL,
  },
  actionAreaCard: {
  }
};

export default ProfilePanel;
