//abhishek360

import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import {
  TextField,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  FormControlLabel,
} from '@material-ui/core';
import * as Colors from '../configs/colors';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import BackgroundImage from '../assests/home_back.png';
// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Link color="inherit" href="https://material-ui.com/">
//         Your Website
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

class Register extends Component {
  state = {
    first_name: '',
    last_name: '',
    phone: '',
    email: '',
    dob: '',
    gender: '',
    password: '',
    confPassword: '',
  }

  verifyRegister = () => {
    const { first_name, last_name, phone, dob, gender, password, confPassword, email } = this.state;
    console.log('dob in sign up', dob);
    if(first_name===''){
      alert('First Name is Required!');
      return;
    }
    else if(email===''){
      alert('Email is Required!');
      return;
    }
    else if(email.search('@')<0){
      alert('Enter a valid Email!');
      return;
    }
    else if(phone===''){
      alert('Phone is Required!');
      return;
    }
    else if(dob ===''){
      alert('Enter a valid Date of Birth!');
      return;
    }
    else if(gender<1){
      alert('Gender is Required!');
      return;
    }
    else if(password===''){
      alert('Password is Required!');
      return;
    }
    else if(password.length<8){
      alert('Password is Too Short!');
      return;
    }
    else if(password!==confPassword){
      alert('Password Mismatch!');
      return;
    }
    else{
      this.props.handleRegister({
        firstName: first_name,
        lastName: last_name,
        email: email,
        phone: phone,
        dob: dob,
        gender: gender,
        password: password
      });
    }

  }

  render(){
    const curDate = new Date();
    const maxDate = (curDate.getFullYear()-16)+'-'+curDate.getMonth()+"-"+curDate.getDate();
    return (
      <div style={styles.container}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={4}>
        </Grid>
        <Grid item xs={12} sm={3}>
        </Grid>
        <Grid item xs={12} sm={5}>
          <Container  component="main" maxWidth='xs'>
            <CssBaseline />
            <div style={styles.paper}>
              <Avatar style={styles.avatar}>
                <ExitToAppIcon />
              </Avatar>
              <Typography component="h1" variant="h4">
                Create an Account
              </Typography>
              <Typography component="h1" variant="h6">
                Be a part of letterBox Community.
              </Typography>
              <br/>
              <br/>
              <div style={styles.form} noValidate>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      autoComplete="fname"
                      onChange = {(event) => this.setState({first_name: event.target.value})}
                      name="firstName"
                      variant="outlined"
                      required
                      fullWidth
                      id="firstName"
                      label="First Name"
                      autoFocus
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      variant="outlined"
                      fullWidth
                      id="lastName"
                      label="Last Name"
                      name="lastName"
                      autoComplete="lname"
                      onChange = {(event) => this.setState({last_name: event.target.value})}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      id="email"
                      label="Email Address"
                      name="email"
                      autoComplete="email"
                      onChange = {(event) => this.setState({email: event.target.value})}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      id="phone"
                      label="Phone No."
                      name="phone"
                      autoComplete="phone"
                      onChange = {(event) => this.setState({phone: event.target.value})}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      onChange = {(event) => this.setState({dob: event.target.value})}
                      name= "dob"
                      type= "date"
                      InputLabelProps= {{
                        shrink: true,
                      }}
                      inputProps={{
                        max: maxDate,
                      }}
                      variant= "outlined"
                      required
                      fullWidth
                      id= "dob"
                      label= "Date of Birth"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <FormControl
                      variant="outlined"
                      required
                      fullWidth
                    >
                      <InputLabel id= 'gender_select_label'>Gender</InputLabel>
                      <Select
                        labelId = 'gender_select_label'
                        id="gender"
                        value = {this.state.gender}
                        labelWidth={70}
                        onChange= {(event) => this.setState({gender: event.target.value})}
                      >
                        <MenuItem value= 'm'>Male</MenuItem>
                        <MenuItem value= 'f'>Female</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      name="password"
                      label="Password"
                      type="password"
                      autoComplete="current-password"
                      onChange = {(event) => this.setState({password: event.target.value})}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      name="confPassword"
                      label="Confirm Password"
                      type="password"
                      autoComplete="current-password"
                      onChange = {(event) => this.setState({confPassword: event.target.value})}
                    />
                  </Grid>
                  <br/>
                  <Grid item xs={12}>
                    <FormControlLabel
                      control={<Checkbox value="agreeTerms" color="primary" />}
                      label="I agree to the terms and conditions of letterBox."
                    />
                  </Grid>
                </Grid>
                <br/>
                <Button
                  fullWidth
                  variant="contained"
                  color="primary"
                  onClick = {() => this.verifyRegister()}
                  style={styles.submit}
                >
                  Sign Up
                </Button>
              </div>
            </div>
          </Container>
        </Grid>
      </Grid>
      </div>
    );
  }
}

const styles = {
  container: {
    width: '100%',
    paddingTop: 50,
    alignItems: 'center',
    backgroundImage: `url(${BackgroundImage})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    minHeight: '95vh',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    backgroundColor: Colors.FOREGROUND,
    marginBottom: 20,
  },
  form: {
    width: '100%',
    marginTop: 10,
  },
  submit: {
    color: 'white',
    background: Colors.FOREGROUND,
  },
}

export default Register;
