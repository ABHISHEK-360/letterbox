//abhishek360

import {
  ADMIN,
  GET_FRIENDS,
  GET_FRIEND_REQUESTS,
  POST_ACCEPT_REQUEST,
  POST_REMOVE_FRIEND,
  POST_ADD_FRIEND
} from '../constants/action-constants';

export const getFriends = ()=>(
  {
    type : GET_FRIENDS,
    route : `users/friend`,
    domain: ADMIN,
  }
)

export const getFriendRequests = ()=>(
  {
    type : GET_FRIEND_REQUESTS,
    route : `users/friend_requests`,
    domain: ADMIN,
  }
)

export const postAcceptRequest = (friendId)=>(
  {
    type : POST_ACCEPT_REQUEST,
    route : `users/accept_friend`,
    domain: ADMIN,
    data: {
      friendId,
    }
  }
)

export const postRemoveFriend = (friendId)=>(
  {
    type : POST_REMOVE_FRIEND,
    route : `users/remove_friend`,
    domain: ADMIN,
    data: {
      friendId,
    }
  }
)

export const postAddFriend = (friendId)=>(
  {
    type : POST_ADD_FRIEND,
    route : `users/friend`,
    domain: ADMIN,
    data: {
      friendId,
    }
  }
)
