//abhishek360

import {
  ADMIN,
  FETCH_USER_DETAILS,
  FETCH_USER_POSTS,
  POST_SEARCH_USER,
  USER_LOGOUT,
  POST_TIMELINE
} from '../constants/action-constants';

export const fetchUserDetails = ()=>(
  {
    type : FETCH_USER_DETAILS,
    route : `users/details`,
    domain : ADMIN,
  }
)

export const postTimeline = (timelineId)=>(
  {
    type : POST_TIMELINE,
    route : `social/timeline`,
    domain : ADMIN,
    data: {
      timelineId
    }
  }
)

export const postSearchUsers = (email)=>(
  {
    type : POST_SEARCH_USER,
    route : `users/search_user`,
    domain : ADMIN,
    data: {
      email
    }
  }
)

export const fetchUserPosts = ()=>(
  {
    type : FETCH_USER_POSTS,
    route : `social/my_post`,
    domain : ADMIN,
  }
)

export const userLogout = ( ) => ({
  type: USER_LOGOUT,
});
