//abhishek360

import {
  ADMIN,
  POST_CREATE_POST,
  POST_REMOVE_POST,
  FETCH_POSTS
} from '../constants/action-constants';

export const postCreatePost = (userId, userName, title, content, likes)=>(
  {
    type : POST_CREATE_POST,
    route : `social/post`,
    domain: ADMIN,
    data: {
      userId,
      userName,
      title,
      content,
      likes
    }
  }
)

export const postRemovePost = (postId)=>(
  {
    type : POST_REMOVE_POST,
    route : `social/remove_post`,
    domain: ADMIN,
    data: {
      postId,
    }
  }
)

export const fetchPosts = ()=>(
  {
    type : FETCH_POSTS,
    route : `social/post`,
    domain : ADMIN,
  }
)
