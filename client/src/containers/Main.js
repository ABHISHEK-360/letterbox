//abhishek360

import React, { Component } from 'react';
import * as Colors from '../configs/colors';
import Home from '../components/Home';
import About from '../components/About';
import UserProfile from '../components/UserProfile';
import Timeline from '../components/Timeline';
import SignUp from '../components/SignUp';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Popup from '../components/Popup';
import RequestService from '../services/RequestService';
import AuthHOC from '../HOC/AuthHOC';
import { Router, Redirect } from '@reach/router';
import { connect } from 'react-redux';
import {
  userLogout,
  fetchUserDetails,
} from '../actions/UserStateActions';

class Main extends Component{
  state = {
    welcome: 'Welcome to Ourforum',
    showPopup: false,
    popupType: 'login'
  }

  constructor(){
    super();
    this.requestService = new RequestService('users','ADMIN')
  }

  async componentDidMount() {
    const loggedIn = await this.requestService.loggedIn();
    if(loggedIn){
      this.props.fetchUserDetails();
    }
  }

  togglePopup = (type) => {
    this.setState({
      popupType: type,
      showPopup: !this.state.showPopup,
    })
  }

  changePopupType = (type) => {
    this.setState({
      popupType: type,
    })
  }

  handleLogin = async ( username, password ) => {
    const data = await this.requestService.auth( username, password );
    console.log('loginnnnnnnnnnnnnnn', data);
    if(data.success){
      //alert('User Logged In!');
      this.props.fetchUserDetails();
      this.togglePopup('');
    }
    else{
      alert('Try Again, Failed to Login!');
    }
  }

  handleRegister = async ( user ) => {
    const data = await this.requestService.reg( user );
    console.log('registeredddddddddddddddddd', data);
    if(data.success){
      alert('User Registered successfully, please login to continue.');
      this.togglePopup('login');
    }
    else{
      alert('Register Failed: ' + data.data);
    }
  }

  handleLogout = async () => {
    await this.requestService.logout();
    this.props.userLogout();
  }

  render(){
    return (
      <div style = { styles.container }>
        <Header
          handleLogout = { this.handleLogout }
          togglePopup = { this.togglePopup }
        />
        {
          this.state.showPopup &&
          <Popup
            type = { this.state.popupType }
            closePopup = { this.togglePopup }
            handleLogin = { this.handleLogin }
            handleRegister = { this.handleRegister }
            changePopupType = { this.changePopupType }
          />
        }
        <Router style = {{paddingTop: 50, paddingBottom: 30 }}>
          <AuthHOC path = '/'
            yes ={ () =>
              <Home
                isEmailVerified = {this.props.userDetails.verification}
              />
            }
            no = {() =>
              <SignUp
                handleRegister = { this.handleRegister }
                changePopupType = { this.changePopupType }
              />
            }
          />
          <About path = 'aboutus'/>
          <AuthHOC path = 'userprofile'
            yes ={ () =>
              <UserProfile/>
            }
            no = {() =>
              <Redirect noThrow to = '/'/>
            }
          />
          <Timeline path = 'timeline/:timelineId'/>
          <AuthHOC path = 'register'
            yes ={ () =>
              <Redirect noThrow to = '/'/>
            }
            no = {() =>
              <SignUp
                handleRegister = { this.handleRegister }
                changePopupType = { this.changePopupType }
              />
            }
          />
        </Router >
        <Footer/>
      </div>
    )
  }
}

const styles = {
  container: {
    minHeight: '97vh',
    backgroundColor: Colors.BACKGROUND,
  },
}

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {
    userLogout,
    fetchUserDetails,
  })(Main);
