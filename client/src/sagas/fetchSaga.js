import { all, takeEvery } from 'redux-saga/effects';

import {
  GET_FRIENDS,
  FETCH_USER_DETAILS,
  FETCH_USER_POSTS,
  GET_FRIEND_REQUESTS,
  FETCH_POSTS,
} from '../constants/action-constants';

import { fetchWorkerSaga }  from './getSagasHelpers';

export default function* fetchSagas() {
  try {
    yield all([
      takeEvery(FETCH_USER_DETAILS, fetchWorkerSaga),
      takeEvery(FETCH_USER_POSTS, fetchWorkerSaga),
      takeEvery(GET_FRIEND_REQUESTS, fetchWorkerSaga),
      takeEvery(GET_FRIENDS, fetchWorkerSaga),
      takeEvery(FETCH_POSTS, fetchWorkerSaga),
    ])
  }
  catch (error) {
    console.log('errors in get sagas ==> ', error);
  }
}
