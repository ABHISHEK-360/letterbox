import { all, takeEvery } from 'redux-saga/effects';

import{
  POST_CREATE_POST,
  POST_REMOVE_POST,
  POST_ACCEPT_REQUEST,
  POST_TIMELINE,
  POST_REMOVE_FRIEND,
  POST_SEARCH_USER,
} from '../constants/action-constants';
import { postWorkerSaga } from './getSagasHelpers';

export default function* postSagas() {
  try {
    yield all([
      takeEvery(POST_CREATE_POST, postWorkerSaga),
      takeEvery(POST_ACCEPT_REQUEST, postWorkerSaga),
      takeEvery(POST_TIMELINE, postWorkerSaga),
      takeEvery(POST_REMOVE_FRIEND, postWorkerSaga),
      takeEvery(POST_REMOVE_POST, postWorkerSaga),
      takeEvery(POST_SEARCH_USER, postWorkerSaga)
    ])
  }
  catch (error) {
    console.log('errors in post sagas ==> ', error);
  }
}
