import {  call, put, } from 'redux-saga/effects';
import RequestService from '../services/RequestService';


const fetchRequest = ({ route, query, domain }) => {
  const fetchData = new RequestService(route, domain);
  return fetchData.get(query);
};

const putRequest = ({ route, data, headers, query, domain }) => {
  const putData = new RequestService(route, domain);
  return putData.put(data, headers, query);
};

const postRequest = ({ route, data, headers, query, domain }) => {
  const postData = new RequestService(route, domain);
  return postData.post(data, query);
};

const loadRequest = ({ route, domain }) => {
  const postData = new RequestService(route, domain);
  return postData.load();
};



function* fetchWorkerSaga({ route, query, domain, type }) {
  try {
    const result = yield call(fetchRequest, { route, query, domain });
    if (result.success) {
      yield put({
        type: `${type}_SUCCESS`,
        payload: result,
        isLoading: false,
      });
    } else {

      yield put({
        type: `${type}_FAILURE`,
        error: result.error,
        isLoading: false,
      });
    }
  } catch (error) {
    yield put({
      type: `${type}_ERROR`,
      error,
      isLoading: false,
    });
  }
}

function* postWorkerSaga({ route, data, type, domain }) {

  try {
    const result = yield call(postRequest, { route, data, domain });
    console.log('post result', result);
    if (result.success) {
      yield put({
        type: `${type}_SUCCESS`,
        payload: result,
        isLoading: false,
      });
    } else {
      yield put({
        type: `${type}_FAILURE`,
        error: result.message,
        isLoading: false,
      });
    }
  } catch (error) {
    yield put({
      type: `${type}_ERROR`,
      error,
      isLoading: false,
    });
  }
}

function* putWorkerSaga({ route, data, stringify, headers, query, domain, type }) {

  try {
    const result = yield call(putRequest, { route, data, stringify, headers, query, domain });
    if (result.success) {
      yield put({
        type: `${type}_SUCCESS`,
        payload: result,
        isLoading: false,
      });
    } else {
      yield put({
        type: `${type}_FAILURE` ,
        error: result.data.error,
        isLoading: false,
      });
    }
  } catch (error) {
    yield put({
      type: `${type}_ERROR`,
      error,
      isLoading: false,
    });
  }
}

function* loadWorkerSaga({ route, domain, type }) {
  try {
    const result = yield call(loadRequest, { route, domain });
      yield put({
        type: `${type}_SUCCESS`,
        payload: result,
        isLoading: false,
      });
  } catch (error) {
    yield put({
      type: `${type}_ERROR`,
      error,
      isLoading: false,
    });
  }
}

export { fetchWorkerSaga, putWorkerSaga, postWorkerSaga, loadWorkerSaga };
