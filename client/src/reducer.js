//abhishek360
import { combineReducers } from 'redux';

import UserDetailsReducer from './reducers/UserDetailsReducer';
import UserPostsReducer from './reducers/UserPostsReducer';
import PostsReducer from './reducers/PostsReducer';
import FriendRequestReducer from './reducers/FriendRequestReducer';
import FriendListReducer from './reducers/FriendListReducer';
import TimelineReducer from './reducers/TimelineReducer';
import RemovePostReducer from './reducers/RemovePostReducer';
import SearchUserReducer from './reducers/SearchUserReducer';


const appReducer = combineReducers({
  userDetails: UserDetailsReducer,
  userPosts: UserPostsReducer,
  posts: PostsReducer,
  friendReq: FriendRequestReducer,
  friendList: FriendListReducer,
  timeline: TimelineReducer,
  postRemove: RemovePostReducer,
  searchList: SearchUserReducer,
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    state = undefined
  }

  return appReducer(state, action)
}

export default rootReducer;
